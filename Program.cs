﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Program.ReadFile();
            int[] unsortedArr = new int[] { 9, 2, 1, 4, 5, 7, 3, 6, 8 };

            for (var ix = 0; ix < unsortedArr.Count(); ix++)
            {
                for (var iy = 0; iy < unsortedArr.Count(); iy++)
                {
                    if (unsortedArr[ix] < unsortedArr[iy])
                    {
                        var tmp = unsortedArr[ix];
                        unsortedArr[ix] = unsortedArr[iy];
                        unsortedArr[iy] = tmp;
                    }

                }

            }
            Console.WriteLine(unsortedArr);


            unsortedArr.ToList().ForEach(x => Console.WriteLine(x));
            Console.ReadLine();


            //fast way
            var fastSortedarr = unsortedArr.OrderBy(x => x).ToArray();
            fastSortedarr.ToList().ForEach(item =>
            {
                Console.WriteLine(item);
            });
            //Console.WriteLine(fastSortedarr.to)

        }
        public static void ReadFile()
        {
            var lines = File.ReadLines("file.txt");
            int[] fullArry = new int[0];
            foreach(var item in lines)
            {
                int[] testArr = item.Split(',').Select(x =>
                {
                    fullArry=fullArry.Append(Int32.Parse(x)).ToArray();
                    return Int32.Parse(x);
            
                }).ToArray();
                
            }
            

           
           
        }
    }
}
